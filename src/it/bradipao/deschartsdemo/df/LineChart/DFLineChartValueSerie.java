package it.bradipao.deschartsdemo.df.LineChart;

import java.util.ArrayList;

public class DFLineChartValueSerie {

    public ArrayList<DFLineChartValue> mPointList = new ArrayList<DFLineChartValue>();

    public float mYmin = 0, mYmax = 1;

    public int curMonth, curYear;

    DFLineChartType type;

    public DFLineChartValueSerie(DFLineChartType type, int curMonth, int curYear) {
        this.type = type;
        this.curMonth = curMonth;
        this.curYear = curYear;
    }

    public ArrayList<DFLineChartValue> getPointList() {
        return mPointList;
    }

    public void addPoint(DFLineChartValue point) {
        if (mPointList.size() > 0) {
            if (point.y > mYmax) mYmax = point.y;
        } else mYmax = point.y;
        mPointList.add(point);
    }

    public int getSize() {
        return mPointList.size();
    }

    public enum DFLineChartType {
        R,
        G
    }
}
