package it.bradipao.deschartsdemo.df.LineChart;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import it.bradipao.deschartsdemo.df.DFCartesianView;

public class DFLineChartView extends DFCartesianView {

    private DFLineChartValueSerie mSeries;

    // objects
    private Paint mPnt = new Paint();
    private Paint mPntFill = new Paint();
    private Path mPathFill;

    public DFLineChartView(Context context) {
        super(context);
        initPaint();
    }

    public DFLineChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initPaint();
    }

    public void onDraw(Canvas cnv) {
        if (mBmp == null) {
            maxNum = 11;
            hasBottomYear = true;
            seriesSize = mSeries.getPointList().size();
            getViewSizes();
            getXYminmax();
            calcYgridRange();
            calcXYcoefs();
            mBmp = Bitmap.createBitmap(p_width, p_height, Bitmap.Config.ARGB_8888);
            mCnv = new Canvas(mBmp);
            drawData();
            drawBorder();
        }
        cnv.drawBitmap(mBmp, 0, 0, null);
    }

    public void setSerie(DFLineChartValueSerie serie) {
        mSeries = serie;
        switch (serie.type){
            case R:
                mPnt = dlPntRed;
                mPntFill = dlPntRed50a;
                break;
            case G:
                mPnt = dlPntGreen;
                mPntFill = dlPntGreen50a;
                break;
        }
        postInvalidate();
    }

    protected void getXYminmax() {
        // calculate minmax
        mYmax = mSeries.mYmax;
        if (ii == 0) {
            mYmin = mSeries.mYmin;
        } else {
            if (mSeries.mYmin < mYmin) mYmin = mSeries.mYmin;
        }
    }

    protected void drawData() {
        DFLineChartValue v;
        float pY;
        float lastX = 0;
        float firstX = 0;
        // iterate through points
        for (jj = 0; jj < mSeries.mPointList.size(); jj++) {
            points.add((int) (sX + jj * aX));
            v = mSeries.mPointList.get(jj);
            pY = v.y;
            if (firstX == 0) {
                mPath1.reset();
                if(pY != DFLineChartValue.NONE){
                    mPath1.moveTo(sX + jj * aX, eY - (pY - bY) * aY);
                    firstX = sX + jj * aX;
                    lastX = sX + jj * aX;
                }
            } else {
                if(pY != DFLineChartValue.NONE){
                    mPath1.lineTo(sX + jj * aX, eY - (pY - bY) * aY);
                    lastX = sX + jj * aX;
                }
            }
        }
        // draw line
        mCnv.drawPath(mPath1, mPnt);
        // create fill path and draw if requested
        mPathFill = new Path(mPath1);
        mPathFill.lineTo(lastX, eY);
        mPathFill.lineTo(firstX, eY);
        mPathFill.close();
        mCnv.drawPath(mPathFill, mPntFill);

        int year = 0, beginYearX = 0, endYearX = 0;
        for (jj = 0; jj < mSeries.mPointList.size(); jj++) {
            v = mSeries.mPointList.get(jj);
            pY = v.y;
            if(pY != DFLineChartValue.NONE){
                mCnv.drawText(String.format("%.1f", pY), sX + jj * aX, eY - (pY - bY) * aY - 4, mPntBlackText);
            }
            if(v.month == mSeries.curMonth && v.year == mSeries.curYear){
                mCnv.drawCircle(sX + jj * aX, eY + p_text_size / 1.4f, p_text_size/2 + 8, mPntFillBlue);
                mCnv.drawText(""+v.month, sX + jj * aX, eY + p_text_size + 2, mPntWhiteText);
                scrollTo(getNearestPointX((int)(sX + jj * aX - viewWidth)) - p_paddleft, scroller.getCurrY());
            }
            else {
                mCnv.drawText(""+v.month, sX + jj * aX, eY + p_text_size + 2, mPntBlackText);
            }
            if(pY != DFLineChartValue.NONE){
                mCnv.drawLine(sX + jj * aX, eY - (pY - bY) * aY, sX + jj * aX, eY, mPntYearLineGray);
                mCnv.drawCircle(sX + jj * aX, eY - (pY - bY) * aY, 3, mPnt);
            }
            if(year == v.year){
                endYearX = (int) (sX + jj * aX);
            }
            else {
                if(year != 0){
                    mCnv.drawLine(beginYearX, eY + p_text_size + 10, endYearX, eY + p_text_size + 10, mPntYearLineGray);
                    mCnv.drawText(""+year, (beginYearX + endYearX) / 2, eY + 2 * p_text_size + 20, mPntGrayText);
                }
                beginYearX = (int) (sX + jj * aX);
                endYearX = (int) (sX + jj * aX);
                year = v.year;
            }
        }
        mCnv.drawLine(beginYearX, eY + p_text_size + 10, endYearX, eY + p_text_size + 10, mPntYearLineGray);
        mCnv.drawText(""+year, (beginYearX + endYearX) / 2, eY + 2 * p_text_size + 20, mPntGrayText);
    }

    protected void calcXYcoefs() {
        if(maxNum < seriesSize){
            aX = dX / seriesSize;
        }
        else {
            aX = dX / maxNum;
        }
        bX = aX / 2;
        aY = dY / Math.abs(mYmaxGrid - mYminGrid);
        bY = mYminGrid;
    }

}
