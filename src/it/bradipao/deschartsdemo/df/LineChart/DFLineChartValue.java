package it.bradipao.deschartsdemo.df.LineChart;

public class DFLineChartValue {

    public static final int NONE = -1;

    public float y = 0;
    public int month = 0;
    public int year = 0;

    public DFLineChartValue(float y, int month, int year) {
        this.y = y;
        this.month = month;
        this.year = year;
    }

}

