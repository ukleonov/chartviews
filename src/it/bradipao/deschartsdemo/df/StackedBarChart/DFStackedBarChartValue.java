package it.bradipao.deschartsdemo.df.StackedBarChart;

public class DFStackedBarChartValue {

    public float y1 = 0;
    public float y2 = 0;
    public int icon = 0;

    public DFStackedBarChartValue(float y1, float y2, int icon) {
        this.y1 = y1;
        this.y2 = y2;
        this.icon = icon;
    }

}