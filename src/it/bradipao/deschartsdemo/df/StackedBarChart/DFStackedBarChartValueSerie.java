package it.bradipao.deschartsdemo.df.StackedBarChart;

import java.util.ArrayList;

public class DFStackedBarChartValueSerie {

    public ArrayList<DFStackedBarChartValue> mPointList = new ArrayList<DFStackedBarChartValue>();

    public float mYmin = 0, mYmax = 1;

    public float summ = 0;

    public DFStackedBarChartValueSerie() {
    }

    public ArrayList<DFStackedBarChartValue> getPointList() {
        return mPointList;
    }

    public void addPoint(DFStackedBarChartValue point) {
        if (mPointList.size() > 0) {
            if (point.y1 > mYmax) mYmax = point.y1;
            if (point.y2 > mYmax) mYmax = point.y2;
        } else {
            if (point.y1 > point.y2) mYmax = point.y1;
            else mYmax = point.y2;
        }
        summ +=point.y1;
        mPointList.add(point);
    }

    public int getSize() {
        return mPointList.size();
    }
}
