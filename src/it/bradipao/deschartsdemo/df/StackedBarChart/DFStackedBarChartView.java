package it.bradipao.deschartsdemo.df.StackedBarChart;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import it.bradipao.deschartsdemo.df.DFCartesianView;

public class DFStackedBarChartView extends DFCartesianView {

    private DFStackedBarChartValueSerie mSeries;
    private int mXnum = 0;
    private float gX = 1;
    Context context;

    public DFStackedBarChartView(Context context){
        super(context);
        this.context = context;
        initPaint();
    }

    public DFStackedBarChartView(Context context,AttributeSet attrs) {
        super(context,attrs);
        this.context = context;
        initPaint();
    }

    public void onDraw(Canvas cnv) {
        if (mBmp == null) {
            maxNum = 8;
            hasBottomIcons = true;
            seriesSize = mSeries.getPointList().size();
            getViewSizes();
            getXYminmax();
            calcYgridRange();
            calcXYcoefs();
            gX = aX / 2;
            mBmp = Bitmap.createBitmap(p_width, p_height, Bitmap.Config.ARGB_8888);
            mCnv = new Canvas(mBmp);
            drawData();
            drawBorder();
        }
        cnv.drawBitmap(mBmp, 0, 0, null);
    }

    public void setSerie(DFStackedBarChartValueSerie serie) {
        mSeries = serie;
        postInvalidate();
    }

    protected void getXYminmax() {
        if (ii==0) {
            mXnum = mSeries.getSize();
            mYmin = mSeries.mYmin;
            mYmax = mSeries.mYmax;
        } else {
            if (mSeries.getSize()>mXnum) mXnum = mSeries.getSize();
            if (mSeries.mYmin<mYmin) mYmin = mSeries.mYmin;
            if (mSeries.mYmax>mYmax) mYmax = mSeries.mYmax;
        }
    }

    protected void drawData() {
        float pY1, pY2, zY;
        for (ii=0;ii<mSeries.mPointList.size();ii++) {
            pY1 = mSeries.mPointList.get(ii).y1;
            pY2 = mSeries.mPointList.get(ii).y2;
            zY = eY+bY*aY;
            if (zY>eY) zY = eY;
            else if (zY<sY) zY = sY;
            if (!Float.isNaN(pY1) && !Float.isNaN(pY2)) {
                points.add((int) (pdd + sX + ii * aX + 1));
                if(pY1 > pY2){
                    mCnv.drawRect(pdd + sX + gX / 2 + ii * aX + 1, zY, pdd + sX + gX / 2 + ii * aX + gX, eY - (pY1 - bY) * aY, mPntFillRed);
                    mCnv.drawRect(pdd + sX + gX / 2 + ii * aX + 1, zY, pdd + sX + gX / 2 + ii * aX + gX, eY - (pY2 - bY) * aY, mPntFillGray);
                    mCnv.drawText(String.format("%.1f", pY1), (pdd + sX + gX + ii*aX + 1/2), eY - (pY1 - bY) * aY - 2, mPntBlackText);
                    mCnv.drawText("+" + String.format("%.1f", (pY1 - pY2)), (pdd + sX + gX + ii*aX + 1/2), eY - (pY2 - bY) * aY + p_text_size + 2, mPntRedText);
                }
                else {
                    mCnv.drawRect(pdd + sX + gX / 2 + ii * aX + 1, zY, pdd + sX + gX / 2 + ii * aX + gX, eY - (pY2 - bY) * aY, mPntFillGreen);
                    mCnv.drawRect(pdd + sX + gX / 2 + ii * aX + 1, zY, pdd + sX + gX / 2 + ii * aX + gX, eY - (pY1 - bY) * aY, mPntFillGray);
                    mCnv.drawText("-" + String.format("%.1f", (pY2 - pY1)), (pdd + sX + gX + ii*aX + 1/2), eY - (pY2 - bY) * aY - 2, mPntGreenText);
                    mCnv.drawText(String.format("%.1f", pY1), (pdd + sX + gX + ii*aX + 1/2), eY - (pY1 - bY) * aY + p_text_size + 2, mPntBlackText);
                }
                mCnv.drawText(String.format("%.0f", (pY1/mSeries.summ)*100) + "%",pdd + sX+bX+ii*aX,eY+p_text_size+2,mPntGrayText);
                Bitmap icon = BitmapFactory.decodeResource(context.getResources(), mSeries.mPointList.get(ii).icon);
                Rect rect = new Rect((int)(pdd + sX + gX / 4 + ii * aX + 1), (int)(zY + p_text_size + 12), (int)(pdd + sX + gX * 0.75 + ii * aX + gX), (int)(zY + p_text_size + 12 + gX * 1.5));
                mCnv.drawBitmap(icon, null, rect, mPntBlackText);
            }
        }
    }

    protected void calcXYcoefs() {
        if(maxNum < seriesSize){
            aX = dX / seriesSize;
        }
        else {
            aX = dX / maxNum;
        }
        bX = aX / 2;
        if(dX >= aX * mXnum){
            pdd = (dX - aX * mXnum) / 2;
        }
        else {
            pdd = 0;
        }
        aY = dY / Math.abs(mYmaxGrid - mYminGrid);
        bY = mYminGrid;
    }

}
