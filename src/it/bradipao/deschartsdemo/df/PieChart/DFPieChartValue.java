package it.bradipao.deschartsdemo.df.PieChart;

public class DFPieChartValue {

    public float y = 0;
    public float p = 0;
    public int color = 0;
    public int icon = 0;

    public float startDegree;
    public float endDegree;
    public float sweepDegree;

    public DFPieChartValue(float y, int color, int icon) {
        this.y = y;
        this.color = color;
        this.icon = icon;
    }

    public DFPieChartValue(float startDegree, float endDegree, DFPieChartValue targetPie){
        this.startDegree = startDegree;
        this.endDegree = endDegree;
        this.sweepDegree = targetPie.sweepDegree;
        this.color = targetPie.color;
    }

    void setDegree(float startDegree, float endDegree){
        this.startDegree = startDegree;
        this.endDegree = endDegree;
    }

}
