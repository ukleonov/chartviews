package it.bradipao.deschartsdemo.df.PieChart;

import java.util.ArrayList;

public class DFPieChartValueSerie {

    public ArrayList<DFPieChartValue> mPointList = new ArrayList<DFPieChartValue>();

    public float summ = 0;

    public DFPieChartValueSerie() {
    }

    public ArrayList<DFPieChartValue> getPointList() {
        return mPointList;
    }

    public void addPoint(DFPieChartValue point) {
        summ += point.y;
        mPointList.add(point);
    }

    public int getSize() {
        return mPointList.size();
    }

    public ArrayList<DFPieChartValue> getLegendList() {
        ArrayList<DFPieChartValue> legendList = new ArrayList<DFPieChartValue>();
        for (int i = 0; i < mPointList.size(); i++){
            if(mPointList.get(i).p <= 5){
                legendList.add(mPointList.get(i));
            }
        }
        return legendList;
    }

    public double getLegendSize(){
        int lSize = 0;
        for (int i = 0; i < mPointList.size(); i++){
            if(mPointList.get(i).p <= 5){
                lSize++;
            }
        }
        return lSize;
    }

}
