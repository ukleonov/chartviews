package it.bradipao.deschartsdemo.df.PieChart;

import android.content.Context;
import android.graphics.*;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import java.util.ArrayList;

/**
 * Created by 1 on 22.01.15.
 */
public class DFPieChartView extends View {

    private DFPieChartValueSerie mSeries = new DFPieChartValueSerie();
    Context context;

    private Paint cirPaint = new Paint();
    private Paint cirInsidePaint = new Paint();
    private Paint mPntBlackText = new Paint();
    private Paint mPntGrayText = new Paint();
    private Point pieCenterPoint;
    private RectF cirRect;
    private RectF cirInsideRect;

    private Canvas mCnv = null;
    private Bitmap mBmp = null;

    int p_background_color = 0xFFEDEDED;
    int text_color_black = 0xFF000000;
    int text_color_gray = 0xFFA2A2A2;
    float p_text_size = dipToPixel(12.0f);
    Typeface mFontText = Typeface.create("sans-serif-condensed", Typeface.NORMAL);

    int p_width = 0;
    int p_height = 0;
    int legend_width = 0;
    int legend_height = 0;
    int legend_begin_y = 0;
    int iconSize = 0;
    int pieRadius;

    public DFPieChartView(Context context) {
        super(context);
        this.context = context;
        initPaint();
    }

    public DFPieChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initPaint();
    }

    protected void onDraw(Canvas cnv) {
        if (mBmp == null) {
            getViewSizes();
            mBmp = Bitmap.createBitmap(p_width + 2 * iconSize, p_height + 2 * iconSize + legend_height, Bitmap.Config.ARGB_8888);
            mCnv = new Canvas(mBmp);
            drawData();
        }
        cnv.drawBitmap(mBmp, 0, 0, null);
    }

    protected void getViewSizes() {
        p_width = getWidth();
        p_height = getHeight();
        iconSize = (int) (1.5 * p_width / 16);
        legend_width = p_width / 11;
        legend_height = (int) (Math.ceil(mSeries.getLegendSize() / 5) * (iconSize + p_text_size + 10));
        legend_begin_y = p_height - legend_height;
        p_height -= legend_height;
        p_height -= 2 * iconSize;
        p_width -= 2 * iconSize;
        if(p_width > p_height){
            pieRadius = (p_height) / 2;
        }
        else {
            pieRadius = (p_width) / 2;
        }
        pieCenterPoint.set(p_width / 2 + iconSize, p_height / 2 + iconSize);
        cirRect.set(pieCenterPoint.x - pieRadius, pieCenterPoint.y - pieRadius, pieCenterPoint.x + pieRadius, pieCenterPoint.y + pieRadius);
        cirInsideRect.set(pieCenterPoint.x - pieRadius / 2, pieCenterPoint.y - pieRadius / 2, pieCenterPoint.x + pieRadius / 2, pieCenterPoint.y + pieRadius / 2);
    }

    protected void drawData() {
        for (int ii = 0; ii < mSeries.mPointList.size(); ii++){
            cirPaint.setColor(mSeries.getPointList().get(ii).color);
            mCnv.drawArc(cirRect, mSeries.getPointList().get(ii).startDegree, mSeries.getPointList().get(ii).sweepDegree, true, cirPaint);
            drawDataText(mSeries.getPointList().get(ii));
        }
        mCnv.drawCircle(pieCenterPoint.x, pieCenterPoint.y, pieRadius / 1.6f, cirInsidePaint);
        for (int ii = 0; ii < mSeries.mPointList.size(); ii++){
            drawPercentText(mSeries.getPointList().get(ii));
            drawIcon(mSeries.getPointList().get(ii));
        }
        drawLegend(mSeries.getLegendList());
    }

    private void drawDataText(DFPieChartValue pieChartValue){
        if(pieChartValue.p >= 5) {
            float angel = (pieChartValue.startDegree + pieChartValue.endDegree) / 2;
            int sth = 1;
            if (angel % 360 > 180 && angel % 360 < 360) {
                sth = -1;
            }
            float x = (float) (pieCenterPoint.x + Math.cos(Math.toRadians(-angel)) * pieRadius / 1.2);
            float y = (float) (pieCenterPoint.y + sth * Math.abs(Math.sin(Math.toRadians(-angel))) * pieRadius / 1.2);
            mCnv.drawText(String.format("%.1f", pieChartValue.y), x, y, mPntBlackText);
        }
    }

    private void drawPercentText(DFPieChartValue pieChartValue){
        if(pieChartValue.p >= 5) {
            float angel = (pieChartValue.startDegree + pieChartValue.endDegree) / 2;
            int sth = 1;
            if (angel % 360 > 180 && angel % 360 < 360) {
                sth = -1;
            }
            float x = (float) (pieCenterPoint.x + Math.cos(Math.toRadians(-angel)) * pieRadius / 2);
            float y = (float) (pieCenterPoint.y + sth * Math.abs(Math.sin(Math.toRadians(-angel))) * pieRadius / 2);
            mCnv.drawText(String.format("%.0f", pieChartValue.y / mSeries.summ * 100) + "%", x, y, mPntGrayText);
        }
    }

    private void drawIcon(DFPieChartValue pieChartValue){
        if(pieChartValue.p >= 5) {
            float angel = (pieChartValue.startDegree + pieChartValue.endDegree) / 2;
            int sth = 1;
            if (angel % 360 > 180 && angel % 360 < 360) {
                sth = -1;
            }
            float x = (float) (pieCenterPoint.x + Math.cos(Math.toRadians(-angel)) * (pieRadius + 0.6 * iconSize));
            float y = (float) (pieCenterPoint.y + sth * Math.abs(Math.sin(Math.toRadians(-angel))) * (pieRadius + 0.6 * iconSize));
            Bitmap icon = BitmapFactory.decodeResource(context.getResources(), pieChartValue.icon);
            Rect rect = new Rect((int) (x - iconSize / 2), (int) (y - iconSize / 2), (int) (x + iconSize / 2), (int) (y + iconSize / 2));
            mCnv.drawBitmap(icon, null, rect, mPntBlackText);
        }
    }

    private void drawLegend(ArrayList<DFPieChartValue> legendList){
        for (int ii = 0; ii < legendList.size(); ii++){
            cirPaint.setColor(legendList.get(ii).color);
            float x = legend_width * (1 + (ii % 5) * 2);
            float y = legend_begin_y + (ii / 5) * (iconSize + p_text_size + 10);
            Bitmap icon = BitmapFactory.decodeResource(context.getResources(), legendList.get(ii).icon);
            Rect rect = new Rect((int) (x), (int) (y), (int) (x + iconSize), (int) (y + iconSize));
            mCnv.drawBitmap(icon, null, rect, mPntBlackText);
            mCnv.drawRect(x - iconSize / 3 - 10, y + iconSize / 3, x - 10, y + 2 * iconSize / 3, cirPaint);
            mCnv.drawText(String.format("%.1f", legendList.get(ii).y), x, y + iconSize + 20, mPntBlackText);
            mCnv.drawText("(" + String.format("%.0f", legendList.get(ii).y) + "%)", x + iconSize * 0.75f, y + iconSize + 20, mPntGrayText);
        }
    }

    protected void initPaint() {
        cirPaint = new Paint();
        cirPaint.setAntiAlias(true);
        cirPaint.setColor(Color.GRAY);
        cirInsidePaint = new Paint();
        cirInsidePaint.setAntiAlias(true);
        cirInsidePaint.setColor(p_background_color);
        mPntBlackText.setColor(text_color_black);
        mPntBlackText.setTypeface(mFontText);
        mPntBlackText.setTextSize(p_text_size);
        mPntBlackText.setStyle(Paint.Style.FILL);
        mPntBlackText.setAntiAlias(true);
        mPntBlackText.setTextAlign(Paint.Align.CENTER);
        mPntGrayText.setColor(text_color_gray);
        mPntGrayText.setTypeface(mFontText);
        mPntGrayText.setTextSize(p_text_size);
        mPntGrayText.setStyle(Paint.Style.FILL);
        mPntGrayText.setAntiAlias(true);
        mPntGrayText.setTextAlign(Paint.Align.CENTER);
        pieCenterPoint = new Point();
        cirRect = new RectF();
        cirInsideRect = new RectF();
        setBackgroundColor(p_background_color);
    }

    public void setSerie(DFPieChartValueSerie serie) {
        initPies(serie);
        mSeries = serie;
        postInvalidate();
    }

    private void initPies(DFPieChartValueSerie serie){
        float totalAngel = 270;
        for(DFPieChartValue pie : serie.getPointList()){
            pie.p = pie.y / serie.summ * 100;
            pie.sweepDegree = pie.y * 360 / serie.summ;
            pie.setDegree(totalAngel, totalAngel + pie.sweepDegree);
            totalAngel += pie.sweepDegree;
        }
    }

    protected float dipToPixel(float dips) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dips, getResources().getDisplayMetrics());
    }
}
