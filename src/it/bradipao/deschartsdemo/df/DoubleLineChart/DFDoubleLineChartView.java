package it.bradipao.deschartsdemo.df.DoubleLineChart;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import it.bradipao.deschartsdemo.df.DFCartesianView;

/**
* Created by 1 on 20.01.15.
*/
public class DFDoubleLineChartView extends DFCartesianView {

    private DFDoubleLineChartValueSerie mSeries;

    // objects
    private Paint mPnt1 = new Paint();
    private Paint mPnt2 = new Paint();
    private Paint mPntFill1 = new Paint();
    private Paint mPntFill2 = new Paint();
    private Paint mPntText1 = new Paint();
    private Paint mPntText2 = new Paint();
    private Path mPathFill;

    public DFDoubleLineChartView(Context context) {
        super(context);
        initPaint();
    }

    public DFDoubleLineChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initPaint();
    }

    public void onDraw(Canvas cnv) {
        if (mBmp == null) {
            maxNum = 11;
            hasBottomYear = true;
            seriesSize = mSeries.getPointList().size();
            getViewSizes();
            getXYminmax();
            calcYgridRange();
            calcXYcoefs();
            mBmp = Bitmap.createBitmap(p_width, p_height, Bitmap.Config.ARGB_8888);
            mCnv = new Canvas(mBmp);
            drawData();
            drawBorder();
        }
        cnv.drawBitmap(mBmp, 0, 0, null);
    }

    public void setSerie(DFDoubleLineChartValueSerie serie) {
        mSeries = serie;
        switch (serie.type){
            case RG:
                mPnt1 = dlPntRed;
                mPnt2 = dlPntGreen;
                mPntFill1 = dlPntRed50a;
                mPntFill2 = dlPntGreen50a;
                mPntText1 = dlPntRedText;
                mPntText2 = dlPntGreenText;
                break;
            case RB:
                mPnt1 = dlPntRed;
                mPnt2 = dlPntBlue;
                mPntFill1 = dlPntRed50a;
                mPntFill2 = dlPntBlue50a;
                mPntText1 = dlPntRedText;
                mPntText2 = dlPntBlueText;
                break;
            case GB:
                mPnt1 = dlPntGreen;
                mPnt2 = dlPntBlue;
                mPntFill1 = dlPntGreen50a;
                mPntFill2 = dlPntBlue50a;
                mPntText1 = dlPntGreenText;
                mPntText2 = dlPntBlueText;
                break;
        }
        postInvalidate();
    }

    protected void getXYminmax() {
        // calculate minmax
        mYmax = mSeries.mYmax;
        if (ii == 0) {
            mYmin = mSeries.mYmin;
        } else {
            if (mSeries.mYmin < mYmin) mYmin = mSeries.mYmin;
        }
    }

    protected void drawData() {
        DFDoubleLineChartValue v;
        float pY1, pY2;
        float firstX1 = 0, firstX2 = 0;
        float lastX1 = 0, lastX2 = 0;
        // iterate through points
        for (jj = 0; jj < mSeries.mPointList.size(); jj++) {
            points.add((int) (sX + jj * aX));
            v = mSeries.mPointList.get(jj);
            pY1 = v.y1;
            pY2 = v.y2;
            if (firstX1 == 0) {
                mPath1.reset();
                if(pY1 != DFDoubleLineChartValue.NONE){
                    mPath1.moveTo(sX + jj * aX, eY - (pY1 - bY) * aY);
                    firstX1 = sX + jj * aX;
                    lastX1 = sX + jj * aX;
                }
            } else {
                if(pY1 != DFDoubleLineChartValue.NONE){
                    mPath1.lineTo(sX + jj * aX, eY - (pY1 - bY) * aY);
                    lastX1 = sX + jj * aX;
                }
            }
            if (firstX2 == 0) {
                mPath2.reset();
                if(pY2 != DFDoubleLineChartValue.NONE){
                    mPath2.moveTo(sX + jj * aX, eY - (pY2 - bY) * aY);
                    firstX2 = sX + jj * aX;
                    lastX2 = sX + jj * aX;
                }
            } else {
                if(pY2 != DFDoubleLineChartValue.NONE){
                    mPath2.lineTo(sX + jj * aX, eY - (pY2 - bY) * aY);
                    lastX2 = sX + jj * aX;
                }
            }
        }
        // create fill path and draw if requested
        mPathFill = new Path(mPath1);
        mPathFill.lineTo(lastX1, eY);
        mPathFill.lineTo(firstX1, eY);
        mPathFill.close();
        mCnv.drawPath(mPathFill, mPntFill1);

        mPathFill = new Path(mPath2);
        mPathFill.lineTo(lastX2, eY);
        mPathFill.lineTo(firstX2, eY);
        mPathFill.close();
        mCnv.drawPath(mPathFill, mPntFill2);

        int year = 0, beginYearX = 0, endYearX = 0;
        for (jj = 0; jj < mSeries.mPointList.size(); jj++) {
            v = mSeries.mPointList.get(jj);
            pY1 = v.y1;
            pY2 = v.y2;
            if(pY1 != DFDoubleLineChartValue.NONE || pY2 != DFDoubleLineChartValue.NONE) {
                if (pY1 > pY2) {
                    mCnv.drawLine(sX + jj * aX, eY - (pY1 - bY) * aY, sX + jj * aX, eY, mPntYearLineGray);
                } else {
                    mCnv.drawLine(sX + jj * aX, eY - (pY2 - bY) * aY, sX + jj * aX, eY, mPntYearLineGray);
                }
            }
            if(pY1 != DFDoubleLineChartValue.NONE){
                mCnv.drawCircle(sX + jj * aX, eY - (pY1 - bY) * aY, 3, mPnt1);
            }
            if(pY2 != DFDoubleLineChartValue.NONE){
                mCnv.drawCircle(sX + jj * aX, eY - (pY2 - bY) * aY, 3, mPnt2);
            }
            if(pY1 > pY2){
                if(pY1 != DFDoubleLineChartValue.NONE){
                    mCnv.drawText(String.format("%.1f", pY1), sX + jj * aX, eY - (pY1 - bY) * aY - 8, mPntText1);
                }
                if(pY2 != DFDoubleLineChartValue.NONE){
                    mCnv.drawText(String.format("%.1f", pY2), sX + jj * aX, eY - (pY2 - bY) * aY + p_text_size + 2, mPntText2);
                }
            }
            else {
                if(pY1 != DFDoubleLineChartValue.NONE){
                    mCnv.drawText(String.format("%.1f", pY1), sX + jj * aX, eY - (pY1 - bY) * aY + p_text_size + 2, mPntText1);
                }
                if(pY2 != DFDoubleLineChartValue.NONE){
                    mCnv.drawText(String.format("%.1f", pY2), sX + jj * aX, eY - (pY2 - bY) * aY - 8, mPntText2);
                }
            }
            if(v.month == mSeries.curMonth && v.year == mSeries.curYear){
                mCnv.drawCircle(sX + jj * aX, eY + p_text_size / 1.4f, p_text_size/2 + 4, mPntFillBlue);
                mCnv.drawText(""+v.month, sX + jj * aX, eY + p_text_size + 2, mPntWhiteText);
                scrollTo(getNearestPointX((int)(sX + jj * aX - viewWidth)) - p_paddleft, scroller.getCurrY());
            }
            else {
                mCnv.drawText(""+v.month, sX + jj * aX, eY + p_text_size + 2, mPntBlackText);
            }
            if(year == v.year){
                endYearX = (int) (sX + jj * aX);
            }
            else {
                if(year != 0){
                    mCnv.drawLine(beginYearX, eY + p_text_size + 10, endYearX, eY + p_text_size + 10, mPntYearLineGray);
                    mCnv.drawText(""+year, (beginYearX + endYearX) / 2, eY + 2 * p_text_size + 20, mPntGrayText);
                }
                beginYearX = (int) (sX + jj * aX);
                endYearX = (int) (sX + jj * aX);
                year = v.year;
            }
        }
        mCnv.drawLine(beginYearX, eY + p_text_size + 10, endYearX, eY + p_text_size + 10, mPntYearLineGray);
        mCnv.drawText(""+year, (beginYearX + endYearX) / 2, eY + 2 * p_text_size + 20, mPntGrayText);
        // draw line
        mCnv.drawPath(mPath1, mPnt1);
        mCnv.drawPath(mPath2, mPnt2);
    }

    protected void calcXYcoefs() {
        if(maxNum < seriesSize){
            aX = dX / seriesSize;
        }
        else {
            aX = dX / maxNum;
        }
        bX = aX / 2;
        aY = dY / Math.abs(mYmaxGrid - mYminGrid);
        bY = mYminGrid;
    }

}
