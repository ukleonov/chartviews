package it.bradipao.deschartsdemo.df.DoubleLineChart;

/**
 * Created by 1 on 20.01.15.
 */
public class DFDoubleLineChartValue {

    public static final int NONE = -1;

    public float y1 = 0;
    public float y2 = 0;
    public int month = 0;
    public int year = 0;

    public DFDoubleLineChartValue(float y1, float y2, int month, int year) {
        this.y1 = y1;
        this.y2 = y2;
        this.month = month;
        this.year = year;
    }

}
