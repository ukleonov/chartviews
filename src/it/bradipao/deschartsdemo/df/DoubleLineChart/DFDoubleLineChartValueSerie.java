package it.bradipao.deschartsdemo.df.DoubleLineChart;

import java.util.ArrayList;

/**
 * Created by 1 on 20.01.15.
 */
public class DFDoubleLineChartValueSerie {

    public ArrayList<DFDoubleLineChartValue> mPointList = new ArrayList<DFDoubleLineChartValue>();

    public float mYmin = 0, mYmax = 1;

    public int curMonth, curYear;

    public DFDoubleLineChartType type;

    public DFDoubleLineChartValueSerie(DFDoubleLineChartType type, int curMonth, int curYear) {
        this.type = type;
        this.curMonth = curMonth;
        this.curYear = curYear;
    }

    public ArrayList<DFDoubleLineChartValue> getPointList() {
        return mPointList;
    }

    public void addPoint(DFDoubleLineChartValue point) {
        if (mPointList.size() > 0) {
            if (point.y1 > mYmax) mYmax = point.y1;
            if (point.y2 > mYmax) mYmax = point.y2;
        } else {
            if (point.y1 > point.y2) mYmax = point.y1;
            else mYmax = point.y2;
        }
        mPointList.add(point);
    }

    public int getSize() {
        return mPointList.size();
    }

    public enum DFDoubleLineChartType {
        RG,
        RB,
        GB
    }
}
