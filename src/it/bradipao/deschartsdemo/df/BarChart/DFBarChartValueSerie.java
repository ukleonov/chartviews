package it.bradipao.deschartsdemo.df.BarChart;

import java.util.ArrayList;

public class DFBarChartValueSerie {

    public ArrayList<DFBarChartValue> mPointList = new ArrayList<DFBarChartValue>();

    public float mYmin = 0, mYmax = 1;

    public float summ = 0;

    public DFBarChartValueSerie() {
    }

    public ArrayList<DFBarChartValue> getPointList() {
        return mPointList;
    }

    public void addPoint(DFBarChartValue point) {
        if (mPointList.size() > 0) {
            if (point.y > mYmax) mYmax = point.y;
        } else mYmax = point.y;
        summ +=point.y;
        mPointList.add(point);
    }

    public int getSize() {
        return mPointList.size();
    }
}
