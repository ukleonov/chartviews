package it.bradipao.deschartsdemo.df.BarChart;

public class DFBarChartValue {

    public float y = 0;
    public int color = 0;
    public int icon = 0;

    public DFBarChartValue(float y, int color, int icon) {
        this.y = y;
        this.color = color;
        this.icon = icon;
    }

}
