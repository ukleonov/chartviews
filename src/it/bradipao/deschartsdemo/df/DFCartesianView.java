package it.bradipao.deschartsdemo.df;

import android.content.Context;
import android.graphics.*;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Scroller;

import java.util.ArrayList;

/**
 * Created by 1 on 31.12.14.
 */
public class DFCartesianView extends View {

    GestureDetector gestureDetector;
    public Scroller scroller;
    public int maxNum;
    public int seriesSize;
    public ArrayList<Integer> points;

    // view params
    public int p_width = 0;
    public int p_height = 0;
    int p_paddtop = 8;
    int p_paddright = 8;
    int p_paddbottom = 8;
    public int p_paddleft = 8;

    // color params
    int p_background_color = 0xFFEDEDED;
    int p_border_color = 0xFF000000;
    int year_line_gray = 0xFFCBCBCB;
    int text_color_black = 0xFF000000;
    int text_color_gray = 0xFFA2A2A2;
    int text_color_white = 0xFFFFFFFF;

    int stacked_bar_green = 0xFF379644;
    int stacked_bar_gray = 0xFFCCCCCC;
    int stacked_bar_red = 0xFFFC5154;
    int cur_month_blue = 0xFF007FF5;

    int dl_red = 0xfffc6f6f;
    int dl_red_50a = 0x7ffc6363;
    int dl_blue = 0xff007ff5;
    int dl_blue_50a = 0x7f57a4ea;
    int dl_green = 0xff509b5b;
    int dl_green_50a = 0x7f2d8e3a;

    // size params
    float p_border_width = dipToPixel(1.0f);
    public float p_text_size = dipToPixel(12.0f);

    // chart area coordinates
    public float sX, sY, dX, dY, eX, eY;
    // limits for X and Y data
    public float mXmin, mXmax, mYmin, mYmax;
    // limits for grid
    public float mXminGrid, mXmaxGrid, mYminGrid, mYmaxGrid, mXdivGrid, mYdivGrid;
    int mXgridNum, mYgridNum;
    // drawing coefficients
    public float aX, bX, aY, bY;

    public float pdd = 0;
    public int viewWidth = 0;

    // objects
    public Canvas mCnv = null;
    public Bitmap mBmp = null;
    Paint mPntBorder = new Paint();
    public Paint mPntBlackText = new Paint();
    public Paint mPntGrayText = new Paint();
    public Paint mPntWhiteText = new Paint();
    public Paint mPntRedText = new Paint();
    public Paint mPntGreenText = new Paint();
    public Paint mPntFillGreen = new Paint();
    public Paint mPntFillGray = new Paint();
    public Paint mPntFillRed = new Paint();
    public Paint mPntFillBlue = new Paint();
    public Paint mPntYearLineGray = new Paint();

    public Paint dlPntRed = new Paint();
    public Paint dlPntRedText = new Paint();
    public Paint dlPntRed50a = new Paint();
    public Paint dlPntBlue = new Paint();
    public Paint dlPntBlueText = new Paint();
    public Paint dlPntBlue50a = new Paint();
    public Paint dlPntGreen = new Paint();
    public Paint dlPntGreenText = new Paint();
    public Paint dlPntGreen50a = new Paint();


    Typeface mFontText = Typeface.create("sans-serif-condensed", Typeface.NORMAL);
    public Path mPath1 = new Path();
    public Path mPath2 = new Path();

    public int ii, jj;
    public boolean hasBottomIcons = false;
    public boolean hasBottomYear = false;

    /**
     * Constructor.
     */
    public DFCartesianView(Context context) {
        super(context);
        initPaint();
        initScrolls(context);
    }

    /**
     * Constructor.
     */
    public DFCartesianView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initPaint();
        initScrolls(context);
    }

    private void initScrolls(Context context) {
        gestureDetector = new GestureDetector(context, new MyGestureListener());
        scroller = new Scroller(context);
//        setHorizontalScrollBarEnabled(true);
//        setVerticalScrollBarEnabled(true);
//        TypedArray a = context.obtainStyledAttributes(R.styleable.View);
//        initializeScrollbars(a);
//        a.recycle();
//        this.setScrollbarFadingEnabled(false);
        points = new ArrayList<Integer>();
    }

    /**
     * Draws the plot.
     */
    public void onDraw(Canvas cnv) {

        // create cache bitmap if necessary
        if ((mBmp == null)) {

            // get view sizes
            getViewSizes();
            // get min,max data ranges
            getXYminmax();
            // calculate the adjusted grid ranges and drawing parameters
            calcXgridRange();
            calcYgridRange();
            // calculate drawing coefficients
            calcXYcoefs();

            // create bitmap and canvas to draw
            mBmp = Bitmap.createBitmap(p_width, p_height, Bitmap.Config.ARGB_8888);
            mCnv = new Canvas(mBmp);

            // draw outer border
            drawBorder();
        }

        // draw bitmap
        cnv.drawBitmap(mBmp, 0, 0, null);
    }

    /**
     * Sets view padding.
     */
    public void setPadding(int paddtop, int padright, int paddbot, int padleft) {
        p_paddtop = paddtop;
        p_paddright = padright;
        p_paddbottom = paddbot;
        p_paddleft = padleft;
    }

    /**
     * Sets view background color.
     */
    public void setBackgroundColor(int color) {
        p_background_color = color;
        super.setBackgroundColor(color);
    }

    /**
     * Inits Paint objects
     */
    protected void initPaint() {
        mPntBorder.setStyle(Paint.Style.STROKE);
        mPntBorder.setColor(p_border_color);
        mPntBorder.setStrokeWidth(p_border_width);

        mPntBlackText.setColor(text_color_black);
        mPntBlackText.setTypeface(mFontText);
        mPntBlackText.setTextSize(p_text_size);
        mPntBlackText.setStyle(Paint.Style.FILL);
        mPntBlackText.setAntiAlias(true);
        mPntBlackText.setTextAlign(Paint.Align.CENTER);
        mPntGrayText.setColor(text_color_gray);
        mPntGrayText.setTypeface(mFontText);
        mPntGrayText.setTextSize(p_text_size);
        mPntGrayText.setStyle(Paint.Style.FILL);
        mPntGrayText.setAntiAlias(true);
        mPntGrayText.setTextAlign(Paint.Align.CENTER);
        mPntWhiteText.setColor(text_color_white);
        mPntWhiteText.setTypeface(mFontText);
        mPntWhiteText.setTextSize(p_text_size);
        mPntWhiteText.setStyle(Paint.Style.FILL);
        mPntWhiteText.setAntiAlias(true);
        mPntWhiteText.setTextAlign(Paint.Align.CENTER);

        mPntGreenText.setColor(stacked_bar_green);
        mPntGreenText.setTypeface(mFontText);
        mPntGreenText.setTextSize(p_text_size);
        mPntGreenText.setStyle(Paint.Style.FILL);
        mPntGreenText.setAntiAlias(true);
        mPntGreenText.setTextAlign(Paint.Align.CENTER);
        mPntRedText.setColor(stacked_bar_red);
        mPntRedText.setTypeface(mFontText);
        mPntRedText.setTextSize(p_text_size);
        mPntRedText.setStyle(Paint.Style.FILL);
        mPntRedText.setAntiAlias(true);
        mPntRedText.setTextAlign(Paint.Align.CENTER);

        dlPntRedText.setColor(dl_red);
        dlPntRedText.setTypeface(mFontText);
        dlPntRedText.setTextSize(p_text_size);
        dlPntRedText.setStyle(Paint.Style.FILL);
        dlPntRedText.setAntiAlias(true);
        dlPntRedText.setTextAlign(Paint.Align.CENTER);
        dlPntBlueText.setColor(dl_blue);
        dlPntBlueText.setTypeface(mFontText);
        dlPntBlueText.setTextSize(p_text_size);
        dlPntBlueText.setStyle(Paint.Style.FILL);
        dlPntBlueText.setAntiAlias(true);
        dlPntBlueText.setTextAlign(Paint.Align.CENTER);
        dlPntGreenText.setColor(dl_green);
        dlPntGreenText.setTypeface(mFontText);
        dlPntGreenText.setTextSize(p_text_size);
        dlPntGreenText.setStyle(Paint.Style.FILL);
        dlPntGreenText.setAntiAlias(true);
        dlPntGreenText.setTextAlign(Paint.Align.CENTER);
        dlPntRed.reset();
        dlPntRed.setStyle(Paint.Style.STROKE);
        dlPntRed.setColor(dl_red);
        dlPntRed.setAntiAlias(true);
        dlPntRed.setStrokeWidth(5);
        dlPntBlue.reset();
        dlPntBlue.setStyle(Paint.Style.STROKE);
        dlPntBlue.setColor(dl_blue);
        dlPntBlue.setAntiAlias(true);
        dlPntBlue.setStrokeWidth(5);
        dlPntGreen.reset();
        dlPntGreen.setStyle(Paint.Style.STROKE);
        dlPntGreen.setColor(dl_green);
        dlPntGreen.setAntiAlias(true);
        dlPntGreen.setStrokeWidth(5);
        dlPntRed50a.reset();
        dlPntRed50a.setStyle(Paint.Style.FILL);
        dlPntRed50a.setColor(dl_red_50a);
        dlPntRed50a.setAntiAlias(false);
        dlPntBlue50a.reset();
        dlPntBlue50a.setStyle(Paint.Style.FILL);
        dlPntBlue50a.setColor(dl_blue_50a);
        dlPntBlue50a.setAntiAlias(false);
        dlPntGreen50a.reset();
        dlPntGreen50a.setStyle(Paint.Style.FILL);
        dlPntGreen50a.setColor(dl_green_50a);
        dlPntGreen50a.setAntiAlias(false);

        mPntFillGreen.reset();
        mPntFillGreen.setStyle(Paint.Style.FILL);
        mPntFillGreen.setColor(stacked_bar_green);
        mPntFillGreen.setAntiAlias(false);
        mPntFillGray.reset();
        mPntFillGray.setStyle(Paint.Style.FILL);
        mPntFillGray.setColor(stacked_bar_gray);
        mPntFillGray.setAntiAlias(false);
        mPntFillRed.reset();
        mPntFillRed.setStyle(Paint.Style.FILL);
        mPntFillRed.setColor(stacked_bar_red);
        mPntFillRed.setAntiAlias(false);
        mPntFillBlue.reset();
        mPntFillBlue.setStyle(Paint.Style.FILL);
        mPntFillBlue.setColor(cur_month_blue);
        mPntFillBlue.setAntiAlias(false);
        mPntYearLineGray.reset();
        mPntYearLineGray.setStyle(Paint.Style.FILL);
        mPntYearLineGray.setColor(year_line_gray);
        mPntYearLineGray.setAntiAlias(false);
        mPntYearLineGray.setStrokeWidth(2);

        setBackgroundColor(p_background_color);
    }

    /**
     * Gets view sizes
     */
    protected void getViewSizes() {
        // params from view
        viewWidth = getWidth();
        if (maxNum < seriesSize) {
            p_width = ((viewWidth - p_paddright - p_paddleft) / maxNum) * seriesSize + p_paddright + p_paddleft;
        } else {
            p_width = viewWidth;
        }
        if(hasBottomYear){
            if (maxNum < seriesSize) {
                p_width -= (maxNum * (eX - sX)) / (16 * seriesSize);
            }
            else {
                p_width -= (eX - sX) / 16;
            }
        }
        p_height = getHeight();
        sX = p_paddleft;
        sY = p_paddtop;
        eX = p_width - p_paddright;
        eY = p_height - p_paddbottom;
        // adjust depending on text labels
        eY -= p_text_size + 20;

        if(hasBottomIcons) {
            if (maxNum < seriesSize) {
                eY -= 1.5 * ((maxNum * (eX - sX)) / (16 * seriesSize));
            }
            else {
                eY -= 1.5 * ((eX - sX) / 16);
            }
        }
        if(hasBottomYear){
            eY -= p_text_size + 10;
        }

        // chart area range
        dX = eX - sX;
        dY = eY - sY - p_text_size;
    }

    /**
     * Sets dummy values for X,Y ranges
     */
    protected void getXYminmax() {
        mXmin = -9;
        mXmax = 9;
        mYmin = -90;
        mYmax = 90;
    }

    /**
     * Automatic calculation of X grid range
     */
    protected void calcXgridRange() {
        // grid step (10's power lower than current range)
        mXdivGrid = (float) Math.pow(10, Math.floor(Math.log10(Math.abs(mXmax - mXmin))));
        // align Xmin to the left-most grid
        mXminGrid = (float) (mXdivGrid * Math.floor(mXmin / mXdivGrid));
        // align Xmax to the right-most grid
        mXmaxGrid = (float) (mXdivGrid * Math.ceil(mXmax / mXdivGrid));
        // gridnum is always between 2 and 10
        mXgridNum = (int) ((mXmaxGrid - mXminGrid) / mXdivGrid);
        // slightly adjust for form-factor, to avoid rectangular grids (square better)
        if ((dX / dY) < 1.2) {
            if (mXgridNum <= 2) mXgridNum *= 5;
            else if (mXgridNum == 3) mXgridNum *= 3;
            else if (mXgridNum <= 5) mXgridNum *= 2;
        } else {
            if (mXgridNum <= 2) mXgridNum *= 6;
            else if (mXgridNum == 3) mXgridNum *= 4;
            else if (mXgridNum == 4) mXgridNum *= 3;
            else if (mXgridNum <= 6) mXgridNum *= 2;
        }
    }

    /**
     * Automatic calculation of Y grid range
     */
    protected void calcYgridRange() {
        // grid step (10's power lower than current range)
        mYdivGrid = (float) Math.pow(10, Math.floor(Math.log10(Math.abs(mYmax - mYmin))));
        // align Ymin to the left-most grid
        mYminGrid = (float) (mYdivGrid * Math.floor(mYmin / mYdivGrid));
        // align Ymax to the right-most grid
        mYmaxGrid = (float) (mYdivGrid * Math.ceil(mYmax / mYdivGrid));
        // gridnum is always between 2 and 10
        mYgridNum = (int) ((mYmaxGrid - mYminGrid) / mYdivGrid);
        // slightly adjust for form-factor, to avoid rectangular grids (square better)
        if ((dY / dX) < 1.2) {
            if (mYgridNum <= 2) mYgridNum *= 5;
            else if (mYgridNum <= 3) mYgridNum *= 3;
            else if (mYgridNum <= 5) mYgridNum *= 2;
        } else {
            if (mYgridNum <= 2) mYgridNum *= 6;
            else if (mYgridNum == 3) mYgridNum *= 4;
            else if (mYgridNum == 4) mYgridNum *= 3;
            else if (mYgridNum <= 6) mYgridNum *= 2;
        }
    }

    /**
     * Calculates drawing coefficients
     */
    protected void calcXYcoefs() {
        aX = dX / Math.abs(mXmaxGrid - mXminGrid);
        bX = mXminGrid;
        aY = dY / Math.abs(mYmaxGrid - mYminGrid);
        bY = mYminGrid;
    }

    /**
     * Draw outer border
     */
    protected void drawBorder() {
        mPath1.reset();
        mPath1.moveTo(sX, sY);
        mPath1.lineTo(sX, eY);
        mPath1.lineTo(eX, eY);
        mCnv.drawPath(mPath1, mPntBorder);
    }

    /**
     * Converts value expressed in dp (device independent pixel) in value
     * expressed in actual display pixel (depends on display metrics).
     */
    protected float dipToPixel(float dips) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dips, getResources().getDisplayMetrics());
    }

    @Override
    public void computeScroll() {
        if (scroller.computeScrollOffset()) {
            int oldX = getScrollX();
            int oldY = getScrollY();
            int x = scroller.getCurrX();
            int y = scroller.getCurrY();
            scrollTo(x, y);
            if (oldX != getScrollX() || oldY != getScrollY()) {
                onScrollChanged(getScrollX(), getScrollY(), oldX, oldY);
            }

            postInvalidate();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_DOWN) {
            if (!scroller.isFinished()) scroller.abortAnimation();
        }

        if (gestureDetector.onTouchEvent(event)) return true;

        if ((event.getPointerCount() == 1) && ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP)) {
            int newScrollX = getScrollX();
            if (getScrollX() < 0) newScrollX = 0;
            else if (getScrollX() > p_width - getWidth()) newScrollX = p_width - getWidth();
            else if (!points.contains(getScrollX())) newScrollX = getNearestPointX(getScrollX()) - p_paddleft;

            if (newScrollX != getScrollX()) {
                scrollBy(newScrollX - getScrollX(), 0);
            }
        }

        return true;
    }

    @Override
    protected int computeHorizontalScrollRange() {
        return p_width;
    }

    @Override
    protected int computeVerticalScrollRange() {
        return p_height;
    }

    public int getNearestPointX(int x) {
        if (points != null && points.size() > 0) {
            int minDistance = Math.abs(points.get(0) - x);
            int newX = points.get(0);
            for (int i = 0; i < points.size(); i++) {
                if (Math.abs(points.get(i) - x) < minDistance) {
                    minDistance = Math.abs(points.get(i) - x);
                    newX = points.get(i);
                }
            }
            return newX;
        }
        return 0;
    }

    private class MyGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            scrollBy((int) distanceX, 0);
            return true;
        }

    }
}
