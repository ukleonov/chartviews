package it.bradipao.deschartsdemo.df;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import it.bradipao.deschartsdemo.df.DoubleLineChart.DFDoubleLineChartValue;
import it.bradipao.deschartsdemo.df.DoubleLineChart.DFDoubleLineChartValueSerie;
import it.bradipao.deschartsdemo.df.DoubleLineChart.DFDoubleLineChartView;
import it.bradipao.deschartsdemo.df.PieChart.DFPieChartValue;
import it.bradipao.deschartsdemo.df.PieChart.DFPieChartValueSerie;
import it.bradipao.deschartsdemo.df.PieChart.DFPieChartView;

/**
 * Created by 1 on 30.12.14.
 */
public class DFActivity extends Activity {

//    DFBarChartView barChartView;
//    DFStackedBarChartView stackedBarChartView;
//    DFDoubleBarChartView doubleBarChartView;
//    DFLineChartView lineChartView;
    DFDoubleLineChartView doubleLineChartView;
    DFPieChartView pieChartView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.df_layout);
//        barChartView = (DFBarChartView) findViewById(R.id.chart);
//        stackedBarChartView = (DFStackedBarChartView) findViewById(R.id.chart2);
//        doubleBarChartView = (DFDoubleBarChartView) findViewById(R.id.chart3);
//        lineChartView = (DFLineChartView) findViewById(R.id.chart4);
        doubleLineChartView = (DFDoubleLineChartView) findViewById(R.id.chart5);
        pieChartView = (DFPieChartView) findViewById(R.id.chart6);

//        DFBarChartValueSerie rr = new DFBarChartValueSerie();
//        rr.addPoint(new DFBarChartValue(99, Color.RED, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(80, Color.YELLOW, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(180, Color.GRAY, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(99, Color.BLUE, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(80, Color.BLACK, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(120, Color.MAGENTA, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(80, Color.GREEN, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(120, Color.CYAN, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(120, Color.MAGENTA, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(99, Color.RED, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(80, Color.YELLOW, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(180, Color.GRAY, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(99, Color.BLUE, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(80, Color.BLACK, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(120, Color.MAGENTA, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(80, Color.GREEN, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(120, Color.CYAN, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(120, Color.MAGENTA, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(99, Color.RED, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(80, Color.YELLOW, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(180, Color.GRAY, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(99, Color.BLUE, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(80, Color.BLACK, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(120, Color.MAGENTA, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(80, Color.GREEN, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(120, Color.CYAN, R.drawable.icon_account_bank_unpressed));
//        rr.addPoint(new DFBarChartValue(120, Color.MAGENTA, R.drawable.icon_account_bank_unpressed));
//        barChartView.setSerie(rr);
//
//
//        DFStackedBarChartValueSerie zz = new DFStackedBarChartValueSerie();
//        zz.addPoint(new DFStackedBarChartValue(3.5f, 3.0f, R.drawable.icon_account_bank_unpressed));
//        zz.addPoint(new DFStackedBarChartValue(3.0f, 2.5f, R.drawable.icon_account_bank_unpressed));
//        zz.addPoint(new DFStackedBarChartValue(2.0f, 2.7f, R.drawable.icon_account_bank_unpressed));
//        zz.addPoint(new DFStackedBarChartValue(1.9f, 1.8f, R.drawable.icon_account_bank_unpressed));
//        zz.addPoint(new DFStackedBarChartValue(1.7f, 2.3f, R.drawable.icon_account_bank_unpressed));
//        zz.addPoint(new DFStackedBarChartValue(2.0f, 2.7f, R.drawable.icon_account_bank_unpressed));
//        zz.addPoint(new DFStackedBarChartValue(2.0f, 2.7f, R.drawable.icon_account_bank_unpressed));
//        zz.addPoint(new DFStackedBarChartValue(2.0f, 2.7f, R.drawable.icon_account_bank_unpressed));
//        zz.addPoint(new DFStackedBarChartValue(2.0f, 2.7f, R.drawable.icon_account_bank_unpressed));
//        zz.addPoint(new DFStackedBarChartValue(2.0f, 2.7f, R.drawable.icon_account_bank_unpressed));
//        zz.addPoint(new DFStackedBarChartValue(2.0f, 2.7f, R.drawable.icon_account_bank_unpressed));
//        zz.addPoint(new DFStackedBarChartValue(2.0f, 2.7f, R.drawable.icon_account_bank_unpressed));
//        zz.addPoint(new DFStackedBarChartValue(2.0f, 2.7f, R.drawable.icon_account_bank_unpressed));
//        stackedBarChartView.setSerie(zz);
//
//        DFDoubleBarChartValueSerie bb = new DFDoubleBarChartValueSerie();
//        bb.addPoint(new DFDoubleBarChartValue(30, 60, Color.RED, Color.BLUE, R.drawable.icon_account_bank_unpressed));
//        bb.addPoint(new DFDoubleBarChartValue(40, 45, Color.YELLOW, Color.BLACK, R.drawable.icon_account_bank_unpressed));
//        bb.addPoint(new DFDoubleBarChartValue(50, 21, Color.GRAY, Color.MAGENTA, R.drawable.icon_account_bank_unpressed));
//        bb.addPoint(new DFDoubleBarChartValue(30, 60, Color.RED, Color.BLUE, R.drawable.icon_account_bank_unpressed));
//        bb.addPoint(new DFDoubleBarChartValue(40, 45, Color.YELLOW, Color.BLACK, R.drawable.icon_account_bank_unpressed));
//        bb.addPoint(new DFDoubleBarChartValue(50, 21, Color.GRAY, Color.MAGENTA, R.drawable.icon_account_bank_unpressed));
//        bb.addPoint(new DFDoubleBarChartValue(30, 60, Color.RED, Color.BLUE, R.drawable.icon_account_bank_unpressed));
//        bb.addPoint(new DFDoubleBarChartValue(40, 45, Color.YELLOW, Color.BLACK, R.drawable.icon_account_bank_unpressed));
//        bb.addPoint(new DFDoubleBarChartValue(50, 21, Color.GRAY, Color.MAGENTA, R.drawable.icon_account_bank_unpressed));
//        bb.addPoint(new DFDoubleBarChartValue(30, 60, Color.RED, Color.BLUE, R.drawable.icon_account_bank_unpressed));
//        bb.addPoint(new DFDoubleBarChartValue(40, 45, Color.YELLOW, Color.BLACK, R.drawable.icon_account_bank_unpressed));
//        bb.addPoint(new DFDoubleBarChartValue(50, 21, Color.GRAY, Color.MAGENTA, R.drawable.icon_account_bank_unpressed));
//        bb.addPoint(new DFDoubleBarChartValue(30, 60, Color.RED, Color.BLUE, R.drawable.icon_account_bank_unpressed));
//        bb.addPoint(new DFDoubleBarChartValue(40, 45, Color.YELLOW, Color.BLACK, R.drawable.icon_account_bank_unpressed));
//        bb.addPoint(new DFDoubleBarChartValue(50, 21, Color.GRAY, Color.MAGENTA, R.drawable.icon_account_bank_unpressed));
//        doubleBarChartView.setSerie(bb);
//
//        DFLineChartValueSerie ss = new DFLineChartValueSerie(DFLineChartValueSerie.DFLineChartType.R, 1, 2015);
//        ss.addPoint(new DFLineChartValue(DFLineChartValue.NONE, 1, 2014));
//        ss.addPoint(new DFLineChartValue(DFLineChartValue.NONE, 2, 2014));
//        ss.addPoint(new DFLineChartValue(DFLineChartValue.NONE, 3, 2014));
//        ss.addPoint(new DFLineChartValue(10, 4, 2014));
//        ss.addPoint(new DFLineChartValue(40, 5, 2014));
//        ss.addPoint(new DFLineChartValue(20, 6, 2014));
//        ss.addPoint(new DFLineChartValue(30, 7, 2014));
//        ss.addPoint(new DFLineChartValue(10, 8, 2014));
//        ss.addPoint(new DFLineChartValue(40, 9, 2014));
//        ss.addPoint(new DFLineChartValue(20, 10, 2014));
//        ss.addPoint(new DFLineChartValue(30, 11, 2014));
//        ss.addPoint(new DFLineChartValue(10, 12, 2014));
//        ss.addPoint(new DFLineChartValue(40, 1, 2015));
//        ss.addPoint(new DFLineChartValue(DFLineChartValue.NONE, 2, 2015));
//        ss.addPoint(new DFLineChartValue(DFLineChartValue.NONE, 3, 2015));
//        ss.addPoint(new DFLineChartValue(DFLineChartValue.NONE, 4, 2015));
//        lineChartView.setSerie(ss);

        DFDoubleLineChartValueSerie nn = new DFDoubleLineChartValueSerie(DFDoubleLineChartValueSerie.DFDoubleLineChartType.GB, 1, 2015);
        nn.addPoint(new DFDoubleLineChartValue(DFDoubleLineChartValue.NONE, DFDoubleLineChartValue.NONE, 1, 2014));
        nn.addPoint(new DFDoubleLineChartValue(DFDoubleLineChartValue.NONE, DFDoubleLineChartValue.NONE, 2, 2014));
        nn.addPoint(new DFDoubleLineChartValue(DFDoubleLineChartValue.NONE, 30, 3, 2014));
        nn.addPoint(new DFDoubleLineChartValue(DFDoubleLineChartValue.NONE, 10, 4, 2014));
        nn.addPoint(new DFDoubleLineChartValue(10, 40, 5, 2014));
        nn.addPoint(new DFDoubleLineChartValue(30, 20, 6, 2014));
        nn.addPoint(new DFDoubleLineChartValue(20, 30, 7, 2014));
        nn.addPoint(new DFDoubleLineChartValue(40, 10, 8, 2014));
        nn.addPoint(new DFDoubleLineChartValue(10, 40, 9, 2014));
        nn.addPoint(new DFDoubleLineChartValue(30, 20, 10, 2014));
        nn.addPoint(new DFDoubleLineChartValue(20, 30, 11, 2014));
        nn.addPoint(new DFDoubleLineChartValue(40, 10, 12, 2014));
        nn.addPoint(new DFDoubleLineChartValue(10, DFDoubleLineChartValue.NONE, 1, 2015));
        nn.addPoint(new DFDoubleLineChartValue(30, DFDoubleLineChartValue.NONE, 2, 2015));
        nn.addPoint(new DFDoubleLineChartValue(DFDoubleLineChartValue.NONE, DFDoubleLineChartValue.NONE, 3, 2015));
        nn.addPoint(new DFDoubleLineChartValue(DFDoubleLineChartValue.NONE, DFDoubleLineChartValue.NONE, 4, 2015));
        doubleLineChartView.setSerie(nn);

        DFPieChartValueSerie uu = new DFPieChartValueSerie();
        uu.addPoint(new DFPieChartValue(5, Color.MAGENTA, R.drawable.icon_account_bank_unpressed));
        uu.addPoint(new DFPieChartValue(10, Color.RED, R.drawable.icon_account_bank_unpressed));
        uu.addPoint(new DFPieChartValue(15, Color.GREEN, R.drawable.icon_account_bank_unpressed));
        uu.addPoint(new DFPieChartValue(2, Color.BLUE, R.drawable.icon_account_bank_unpressed));
        uu.addPoint(new DFPieChartValue(2, Color.BLACK, R.drawable.icon_account_bank_unpressed));
        uu.addPoint(new DFPieChartValue(2, Color.YELLOW, R.drawable.icon_account_bank_unpressed));
        uu.addPoint(new DFPieChartValue(2, Color.MAGENTA, R.drawable.icon_account_bank_unpressed));
        uu.addPoint(new DFPieChartValue(2, Color.GRAY, R.drawable.icon_account_bank_unpressed));
        uu.addPoint(new DFPieChartValue(2, Color.CYAN, R.drawable.icon_account_bank_unpressed));
        uu.addPoint(new DFPieChartValue(2, Color.DKGRAY, R.drawable.icon_account_bank_unpressed));
        uu.addPoint(new DFPieChartValue(2, Color.LTGRAY, R.drawable.icon_account_bank_unpressed));
        uu.addPoint(new DFPieChartValue(2, Color.WHITE, R.drawable.icon_account_bank_unpressed));
        uu.addPoint(new DFPieChartValue(2, Color.TRANSPARENT, R.drawable.icon_account_bank_unpressed));
        pieChartView.setSerie(uu);

    }
}
