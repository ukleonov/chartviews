package it.bradipao.deschartsdemo.df.DoubleBarChart;

public class DFDoubleBarChartValue {

    public float y1 = 0;
    public float y2 = 0;
    public int color1 = 0;
    public int color2 = 0;
    public int icon = 0;

    public DFDoubleBarChartValue(float y1, float y2, int color1, int color2, int icon) {
        this.y1 = y1;
        this.y2 = y2;
        this.color1 = color1;
        this.color2 = color2;
        this.icon = icon;
    }
}
