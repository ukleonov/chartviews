package it.bradipao.deschartsdemo.df.DoubleBarChart;

import android.content.Context;
import android.graphics.*;
import android.util.AttributeSet;
import it.bradipao.deschartsdemo.df.DFCartesianView;

public class DFDoubleBarChartView extends DFCartesianView {

    private DFDoubleBarChartValueSerie mSeries;
    private int mXnum = 0;
    private float gX = 1;
    Context context;

    private Paint mPntFill1 = new Paint();
    private Paint mPntFill2 = new Paint();

    public DFDoubleBarChartView(Context context){
        super(context);
        this.context = context;
        initPaint();
    }

    public DFDoubleBarChartView(Context context,AttributeSet attrs) {
        super(context,attrs);
        this.context = context;
        initPaint();
    }

    public void onDraw(Canvas cnv) {
        if (mBmp==null) {
            maxNum = 4;
            hasBottomIcons = true;
            seriesSize = mSeries.getPointList().size();
            getViewSizes();
            getXYminmax();
            calcYgridRange();
            calcXYcoefs();
            gX = aX / 4;
            mBmp = Bitmap.createBitmap(p_width, p_height, Bitmap.Config.ARGB_8888);
            mCnv = new Canvas(mBmp);
            drawData();
            drawBorder();
        }
        cnv.drawBitmap(mBmp, 0, 0, null);
    }

    public void setSerie(DFDoubleBarChartValueSerie serie) {
        mSeries = serie;
        postInvalidate();
    }

    protected void getXYminmax() {
        if (ii==0) {
            mXnum = mSeries.getSize();
            mYmin = mSeries.mYmin;
            mYmax = mSeries.mYmax;
        } else {
            if (mSeries.getSize()>mXnum) mXnum = mSeries.getSize();
            if (mSeries.mYmin<mYmin) mYmin = mSeries.mYmin;
            if (mSeries.mYmax>mYmax) mYmax = mSeries.mYmax;
        }
    }

    protected void drawData() {
        float pY1,pY2,zY;
        for (ii=0;ii<mSeries.mPointList.size();ii++) {
            mPntFill1.reset();
            mPntFill1.setStyle(Paint.Style.FILL);
            mPntFill1.setColor(mSeries.getPointList().get(ii).color1);
            mPntFill1.setAntiAlias(false);
            mPntFill2.reset();
            mPntFill2.setStyle(Paint.Style.FILL);
            mPntFill2.setColor(mSeries.getPointList().get(ii).color2);
            mPntFill2.setAntiAlias(false);
            pY1 = mSeries.mPointList.get(ii).y1;
            pY2 = mSeries.mPointList.get(ii).y2;
            zY = eY+bY*aY;
            if (zY>eY) zY = eY;
            else if (zY<sY) zY = sY;
            if (!Float.isNaN(pY1) && !Float.isNaN(pY2)) {
                points.add((int) (pdd + sX + ii * aX + 1));
                mCnv.drawRect(pdd + sX + gX + ii * aX + 1, zY, pdd + sX + gX * 2 + ii * aX, eY - (pY1 - bY) * aY, mPntFill1);
                mCnv.drawRect(pdd + sX + ii * aX + gX * 2.25f, zY, pdd + sX + ii * aX + gX * 3.25f, eY - (pY2 - bY) * aY, mPntFill2);
                mCnv.drawText(String.format("%.1f", pY1), (pdd + sX + gX * 1.5f + ii*aX + 1/2), eY-(pY1-bY)*aY-2, mPntBlackText);
                mCnv.drawText(String.format("%.1f", pY2), (pdd + sX + gX * 2.75f + ii*aX + 1/2), eY-(pY2-bY)*aY-2, mPntBlackText);
                mCnv.drawText(String.format("%.0f", (pY1/mSeries.summ1)*100) + "%",(pdd + sX + gX * 1.5f + ii*aX + 1/2),eY+p_text_size+2,mPntGrayText);
                mCnv.drawText(String.format("%.0f", (pY2/mSeries.summ2)*100) + "%",(pdd + sX + gX * 2.75f + ii*aX + 1/2),eY+p_text_size+2,mPntGrayText);
                Bitmap icon = BitmapFactory.decodeResource(context.getResources(), mSeries.mPointList.get(ii).icon);
                Rect rect = new Rect((int)(pdd + sX + gX * 1.375 + ii * aX + 1), (int)(zY + p_text_size + 12), (int)(pdd + sX + gX * 2.875 + ii * aX + 1), (int)(zY + p_text_size + 12 + gX * 1.5));
                mCnv.drawBitmap(icon, null, rect, mPntBlackText);
            }
        }
    }

    protected void calcXYcoefs() {
        if(maxNum < seriesSize){
            aX = dX / seriesSize;
        }
        else {
            aX = dX / maxNum;
        }
        bX = aX/2;
        if(maxNum > seriesSize){
            pdd = (dX - aX*mXnum) / 2 + bX/2;
        }
        else {
            pdd = 0;
        }
        aY = dY/Math.abs(mYmaxGrid-mYminGrid);
        bY = mYminGrid;
    }

}
