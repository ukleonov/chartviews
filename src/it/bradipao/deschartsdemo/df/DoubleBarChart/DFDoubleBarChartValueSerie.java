package it.bradipao.deschartsdemo.df.DoubleBarChart;

import java.util.ArrayList;

public class DFDoubleBarChartValueSerie {

    public ArrayList<DFDoubleBarChartValue> mPointList = new ArrayList<DFDoubleBarChartValue>();

    public float mYmin = 0, mYmax = 1;

    public float summ1 = 0, summ2 = 0;

    public DFDoubleBarChartValueSerie() {
    }

    public ArrayList<DFDoubleBarChartValue> getPointList() {
        return mPointList;
    }

    public void addPoint(DFDoubleBarChartValue point) {
        if (mPointList.size() > 0) {
            if (point.y1 > mYmax) mYmax = point.y1;
            if (point.y2 > mYmax) mYmax = point.y2;
        } else {
            if (point.y1 > point.y2) mYmax = point.y1;
            else mYmax = point.y2;
        }
        summ1 +=point.y1;
        summ2 +=point.y2;
        mPointList.add(point);
    }

    public int getSize() {
        return mPointList.size();
    }
}
